<?php

$info = posix_getpwuid(posix_getuid());
$homeDirectory = $info['dir'];
$baseDirectory = $homeDirectory . '/managed';
$installationDirectory = $baseDirectory.'/services/appmanager/live/';
try {
    if (is_dir($installationDirectory)) {
        throw new Exception('Installation directory exists');
    }

    // generate directories
    make_directory($installationDirectory.'/version');
    make_directory($installationDirectory.'/data/cache/git');
    make_directory($installationDirectory.'/setup/overrides/config/autoload');

    // create the config override recording base location for user/subscription
    file_put_contents($installationDirectory.'/setup/overrides/config/autoload/setbase.local.php',"<?php
    declare(strict_types=1);
    \$info = posix_getpwuid(posix_getuid());
    \$homeDirectory = realpath(\$info['dir']);
    defined('BASE_DIRECTORY') || define('BASE_DIRECTORY', \$homeDirectory . '/managed');
    return [];
    ");

    // download the git repo for tha appmanager to dummy directory (renamed later)
    `git clone git@gitlab.com:gidato-utils/appmanager $installationDirectory/version/installed`;

    if (!is_dir($installationDirectory . '/version/installed')) {
        throw new Exception('Failed to clone git repo');
    }

    // get the latest version of the app
    $tags = `cd $installationDirectory/version/installed && git tag`;
    $latest = latest_version(explode("\n",$tags));

    // checkout the latest
    `cd $installationDirectory/version/installed && git checkout $latest`;

    // rename the directory to mathc the version number
    rename($installationDirectory.'/version/installed', $installationDirectory.'/version/'.$latest);

    // move the git repo to cache (to save re-loading it later)
    rename($installationDirectory.'/version/'.$latest.'/.git',$installationDirectory.'/data/cache/git/appmanager.git' );

    // build the appmanager app
    // start by creating the local data and linking to the environment cache
    make_directory("$installationDirectory/version/$latest/data");
    symlink("$installationDirectory/data/cache","$installationDirectory/version/$latest/data/cache");

    // copy the local overrides
    `cp -r $installationDirectory/setup/overrides/* $installationDirectory/version/$latest`;

    // run composer (nodev)
    `cd $installationDirectory/version/$latest && composer install --no-suggest --no-interaction --no-dev`;

    // link it into users BIN directory
    symlink("$installationDirectory/version/$latest","$installationDirectory/version/site_current");
    make_directory($homeDirectory.'/bin');
    symlink("$installationDirectory/version/site_current/bin/manager.php",$homeDirectory.'/bin/appmanager');

    // make runnable
    `chmod +x $installationDirectory/version/site_current/bin/manager.php`;

    // then run the setup
    $argv=["$homeDirectory/bin/appmanager",'setup'];
    include("$homeDirectory/bin/appmanager");

} catch (Exception $e) {
    define('NO_COLOR',"\e[0m"); # No Color
    define('RED',"\e[0;31m");
    print RED . $e->getMessage(). NO_COLOR . "\n";
    exit(1);
}

function make_directory(string $directory) : void
{
    if (is_dir($directory)) {
        return;
    }

    make_directory(dirname($directory));
    if (!mkdir($directory, 0755)) {
        throw new Exception('Failed to make directory');
    }
}

function latest_version(array $versions) : string
{
    $latest = 'v0.0.0';
    foreach ($versions as $version) {
        $version = trim($version);
        if (compare_versions($latest, $version) < 1) {
            $latest = $version;
        }
    }
    return $latest;
}

function compare_versions(string $versionA, string $versionB) : int
{
    $partsA = explode('.',ltrim($versionA,'v'));
    $partsB = explode('.',ltrim($versionB,'v'));
    for ($i = 0; $i < 3; $i++) {
        if ($partsA[$i] < $partsB[$i]) {
            return -1;
        }
        if ($partsA[$i] > $partsB[$i]) {
            return 1;
        }
    }
    return 0;
}
